<div align="center">

![mxs-logo-bw-250x114](../Images/mxs-logo-bw-250x114.gif "The Mixed Signals logo is property of Mixed Signals Media.")\

</div>

**Stardrifter Role-Playing Game Base Rulebook** v0.04.3

This edition **© 2021** by **David Collins-Rivera**

Cover Art **© 2021** by **Ignatz**

Chief Engineer: **Lyle McKarns (x1101)**

This book is released under [Creative Commons Attribution Share-Alike International 4.0 License](https://creativecommons.org/licenses/by-sa/4.0/), except for the **Mixed Signals** logo and company assets, which are reserved.

Published by [Mixed Signals Media](http://mixedsignals.ml/)

All internal artwork is in the **Public Domain**, and can be found on
[Freesvg.org](https://freesvg.org/), and other places around the Internet.

Check out [Cavalcadeaudio.com](http://www.cavalcadeaudio.com/), and
discover a whole new galaxy of adventure!
