# Equipment

As outlined in the **Money** section, characters start the game with a certain number of ***Q***, with which they may purchase equipment.

Equipment categories are broken down into *Armor*, *Weapons*, and *Gear*. Naturally enough, certain Skills are best performed in conjunction with certain types of equipment.

NOTE: The Weapons listed below, and some Armor types, refer to something called a ***Weapon DC***, with a rating expressed in numbers (***Weapon DC: 1***, ***Weapon DC: 2***, etc.) This refers to the ***Weapon Damage Class***, which is described in the chapter on **Combat**. For now, just make a note of this on your character sheet, for any item you purchase.

<br />

------------------------------------------------------------------------

## Armor

<br />

#### Ballistic Shield
PR-2, SR-30, Cost: 75***Q***, Opponents have -1 penalty on their Combat Skill score as a situational modifier.

Designed for use with a wide range of firearms, these shields have viewports, slots, and notches so the wielder can fire both pistols and rifles while crouching behind them. Not heavy, but large and awkward. **-2** on DEX Attribute Checks while being used (*not* Skill Checks). A Ballistic Shield can be used with other Armor types, in which case its PR-2 is added to that other Armor's PR score. Attackers have a **-1** situational modifier penalty to their Combat Skill score when attacking anyone using a Ballistic Shield, since it acts as partial cover. User is immune to ***Weapon DC-2*** attacks that come from the front.

<br />

#### Extensible Ballistic Shield
PR-2, SR-20, Cost: 300*Q*, Opponents have **-1** penalty on their Combat Skill score as a situational modifier.

Like the Ballistic Shield above, but this piece of armor straps to the forearm, and snaps open or closed when desired. Because of this, it incurs no DEX penalty for awkwardness. It's not as durable as a solid Ballistic Shield, so it has a smaller SR. It is also much more expensive. This can be used with other Armor types, in which case, its PR-2 is added to that other Armor's PR score. Attackers have a **-1** situational modifier penalty to their Combat Skill score when attacking anyone using an Extensible Ballistic Shield, since it acts as partial cover. User is immune to ***Weapon DC-1*** attacks that come from the front.

<br />

#### Chest Plate
PR-3, SR-30, Cost: 600***Q*** +2 bonus on ***SAVE: Physical*** Checks

This is a hard vest, made of a tough ballistic material. It covers the chest and back.

<br />

#### Flak Vest
PR-2, SR-20, Cost: 400***Q***

A Flak vest is relatively thin, and can be worn under street clothes or other Armor types. It protects the chest and back. If worn under other Armor, its PR-2 is added to the PR of the other Armor.

<br />

#### Flex Vest
PR-2, SR-25, Cost: 500***Q***, +1 bonus on ***SAVE: Physical*** Checks

This is a fairly substantial vest made of a ballistic oobleck; it is soft and flexible to slow contact, but becomes hard and ridged when struck or impacted quickly. It protects the chest and back, but is too bulky to be worn under street clothes without drawing attention.

<br />

#### Hardsuit
PR-4, SR-100, Cost: 800***Q***, +3 bonus on ***SAVE: Physical*** Checks, wearer is immune to Stunner Pistols and similar attacks (stun effects of grenades, etc.).

This is full-body ballistic armor, including helmet and boots. Hardsuits have integrated radio communications, equal to a Radio Headset. Wearer is immune to ***Weapon DC-1*** attacks.

**NOTE**: A Hardsuit does not provide pressuresuit benefits, though a ***Simple Vacsuit*** can fit over it.

<br />

#### Powered Armor
PR-6, SR-200, Cost: 1500***Q***, +4 bonus on ***SAVE: Physical*** Checks, +2 bonus on **SAVE: Mental** Checks, +2 to wearer's **STR** Checks while worn; wearer is immune to stunner pistols and similar attacks, wearer is immune to ***Weapon DC-2]*** or less. Prereq: ***Exosuit***.

This is a full-body exoskeleton, with ballistic plates head-to-toe, and augmentation to the wearer's STR. Powered Armor can act as a fully-functional pressuresuit, with environmental systems, decent protection from cosmic and stellar radiation, and air for up to 24 hours. It is effective against physical, biological, radiological, and directed energy attacks. It has integrated media communications, equal to a Radio Headset, but it also includes internal and external audio/video feeds.

Making active use of Powered Armor, or even just taking a step in it without falling down, requires the ***Exosuit*** Skill, and Skill Checks are required when attempting to do something difficult in stressful circumstances (such as when being attacked).

<br />

------------------------------------------------------------------------

<br />

## Weapons

All Weapons on this short list are of the Civilian Class, and are legal in most (though not all) parts of space. (See the chapter on **Combat**, for details on how some of these are used.)

<br />

#### Club (large)
Damage: **1d6+2**. Cost: 15***Q***. ***Weapon DC-2***. On a roll of 5 or 6 on the damage die, the target must roll a ***SAVE: Physical*** or be Stunned for **1d4** rounds.

This is a large hard-plastic stick that is the size of a bat, or a big riot baton. It cannot be carried on a belt, but it can be strapped to the back, or slung from one shoulder.

<div align="center">

![club\_small-102x2015](../Images/club_small-102x2015.gif "I call this one Ol' Migraine!")\

</div>

#### Club (small)
Damage: **1d4+2**. Cost: 10***Q***. ***Weapon DC-1***. On a full damage hit, the target must roll a ***SAVE: Physical*** or be Stunned for **1d4** rounds. 

This is a short, heavy, hard-plastic club, like a police baton. Can be worn comfortably on a belt, or hidden up a sleeve.

<br />

#### Club (stun baton)
Damage: **1d4+2 plus Stun**. Cost: 300***Q***. ***Weapon DC-1***. On a full damage hit, the target must roll a ***SAVE: Physical*** or be Stunned for **1d4** rounds from bludgeoning damage. Additionally, this weapon can dispense an electrical surge upon a successful hit, regardless of the amount of damage inflicted, which *also* requires the target to roll a ***SAVE: Physical*** or be stunned for **1d4** rounds.

This weapon is the size and shape of a ***Club (small)***. It holds three charges, after which it acts as a normal ***Club (small)*** in combat. The user can choose to make a stun attack or not (there's a button on the side), and if the attack is not successful, the charge is not expended. Also, the attacker does not need to actually do bludgeon
damage with this weapon in order to dispense the electric jolt; a mere touch will do it. This weapon can be recharged from a standard outlet at a rate of **1** charge per minute.

**NOTE**: The stun effects of this weapon are cumulative. Should the attacker roll max damage to hit, while also expending a charge, *and* the defender fails both of their ***SAVE: Physical*** rolls (the second of which will be at **1/2** normal, due to the *first* Stunning), the defender immediately drops unconscious for 1 hour.

**NOTE**: This item is a favorite among police and riot squads, as well as criminal gangs. As such, it is sometimes illegal for ordinary citizens to carry one, though local laws vary.

<br />

#### Hard Object (small)
Damage: **1d4+1**. Cost: n/a. ***Weapon DC-1***. On a full damage hit, the target must roll a ***SAVE: Physical*** or be Stunned for **1d4** rounds.

This refers to any improvised weapon one might find on hand during combat, such as a bottle, a chair, a fire extinguisher, a wrench, a rock, etc. Some of these can be thrown, some not. An item like a glass bottle used in a fight will get smashed; at the GM's option, it leaves behind a broken bottle that does the same amount of damage as before, but otherwise acts like a ***Knife***. Be creative: improvised weapons require improvised rules.

<br />

#### Hard Object (large)
(See the section on *Impact Damage*, in the chaper on ***Damage and Healing***.)

<br />

#### Knife
Damage: **1d4+2**. Cost: 100***Q***. ***Weapon DC-2***}. Special: Inflicting 4 or more **HP** (not Stamina) of damage in one attack, after **Armor PR** has been subtracted, the target must roll a ***SAVE: Physical*** or lose **1 HP** per round until they receive Medico treatment.

This is either a folding or non-folding knife with a moderate blade length and sharp edge.

<br />

#### Mini-Grenade
Damage: **2d4+2**. Cost: 200***Q*** each. ***Weapon DC-3***. Can be thrown out to a range of 30 meters; roll STR Attribute Check to hit. AoE: 4 meter radius. Single use (obviously). In addition to any damage, targets must roll a successful ***SAVE: Physical*** or be Stunned for **1d4** rounds. Roll damage and ***SAVE: Physical*** Checks separately for all targets in the AoE.

This is a miniature stick-shaped explosive device, something like a blasting cap, designed for self-defense against street gangs and small mobs. It can be attached to other objects in order to do minor structural damage, such as blowing open doors, etc.

**NOTE**: Those with the ***Combat: Thrown Object*** Skill (or possibly ***Athleticism***, at the GM's option) may opt to use that instead of making a STR Check to hit, in which case, their range for throwing Mini-Grenades is 30 meters plus 10 meters per Skill level.

<br />

#### Pistol (firearm)
Damage: **1d6+2**. Ammo Capacity: 20 round magazine. Cost: (Pistol) 500***Q***, (Extra magazine) 10**Q**. ***Weapon DC-3***.

A semi-automatic pistol, firing chemically-propelled kinetic slugs. Practical range is 20 meters, though longer shots are entirely possible (with modifiers).

After making an attack with this weapon, whether it's successful or not, roll **1d8** for the number of rounds expended.

<br />

#### Pistol (stunner)
Damage: n/a. Ammo Capacity: 1 charge (disposable/single use) Cost: 150***Q***. ***Weapon DC-1***. Upon a successful attack, the target must roll a ***SAVE: Physical*** or drop unconscious for 1 hour; otherwise they are Stunned for **1d4** rounds.

This is a small, palm-sized energy weapon strictly designed for self-defense. Range is 5 meters.

<br />

#### Rifle (firearm)
Damage: **1d8+2**. Ammo Capacity: 50 round magazine. Cost: (Rifle) 1000***Q***. (Extra magazine): 20***Q***. **Weapon DC-3**.

A larger semi-automatic weapon, firing chemically-propelled kinetic slugs. Practical range is 200 meters, though longer shots are entirely possible (with modifiers).

After making an attack with this weapon, whether it's successful or not, roll **1d12** for the number of rounds
expended.

<br />

#### Sword (small)
Damage: **1d6+2**. Cost: 300***Q***. ***Weapon DC-2***.

Special: Inflicting 4 or more **HP** (not Stamina) of damage in one attack, after **Armor PR** has been subtracted, the target must roll a ***Save vs Physical*** or lose **1 HP** per round until receiving Medico treatment. Cost: 300***Q***

<br />

------------------------------------------------------------------------

<br />


## Gear

<br />

#### Comm
Cost: 1000***Q***

Advanced, portable communication and computing equipment. This can take almost any form, from implantable interfaces, to discreet devices. The exact form is the player's choice, but it must be noted what form it takes upon the Character Sheet. A comm allows direct device-to-device communication, link-ups with information nets, data storage, computational and hacking/cracking functions. A character with the ***Computers*** Skill, who also owns a Comm unit, always has a powerful computer handy.

<div align="center">

![comm-150x188](../Images/comm-150x188.gif "Time to use my Computational Televerbalizer!")\

</div>

#### Medico Kit
Cost: 200***Q***

Provides a +2 for all ***Medico Skill Checks***, and an additional +1 point of healing (STAM or HP) for each ***Medico*** Skill level of the user. Additionally, if a character attempts to stabilize someone who is at or below 0 HP while using a Medico Kit, even if they fail their Skill Check, the victim is considered to be stabilized enough that they do not need continual attendance (See ***Medico*** under the chapter on Skills). A Medico Kit has room for medicines and wound patches for up to 10 uses, successful or not, at which time it needs to be replaced.

<br />

#### Pressuresuit
Cost: 1000***Q***

Provides air and Terran-normal pressurization for up to 24 hours, as well as fine protection from solar and cosmic radiation, and full protection against cold and heat, ranging from 90 to 2000 degrees Kelvin (approx. -180C/-300F to 1730C/3140F). Has integrated media communications, including internal and external audio/video feeds (equal to ***Radio Earbud/Headset***); waste capture; and various bladders spaced around the suit, adding up to 2 liters of water, available to the wearer through a tiny sipping line in the helmet. Though this is not classified as a form of Armor, it nonetheless has PR-1 and SR-10. Once its Structural Rating has run out, it will leak atmo, and the cooling/heating will fail. The wearer can use certain types of Armor with a Pressuresuit, including a Shield, a Flak Vest, or a Chest Plate. While simply wearing a ***Pressuresuit*** does not require the ***Exosuit*** Skill, properly donning and maintaining one *does*, either in the wearer themself, or in someone close by who can assist. If this Skill is not available, the wearer must successfully roll an Attribute Check versus STR at -2. Other modifiers might apply, and a failure here may well be fatal.

<br />

#### Radio Earbud/Headset
Cost: 100***Q***

Provides easily encrypted audio communication up to 200 kilometers. This item can patch into local networks of space stations and other settlements, to provide audio communication anywhere that the network reaches.

<div align="center">

![radio-headset-150x266](../Images/radio-headset-150x266.gif "I'm reading you loud and clear!")\

</div>

#### Science Field Kit
Cost: 150***Q***

This is a general testing kit small enough to fit in a shoulder bag. It allows for basic scientific inquiries away from the lab. A ***Science Field Kit*** provides +2 on all ***Science*** Skill Checks where it is used, involving such things as collecting samples, running simple tests, or taking readings. This kit has volatile chemicals, delicate sensors, and other contents, and can only be actively used up to 10 times before it must be replaced.

<br />

#### Simple Vacsuit
Cost: 500***Q***

This is an emergency suit thin enough to fold into a small pack. It allows a character to survive complete vacuum conditions for up to eight hours, at which time it will run out of air. It will last indefinitely if an outside air supply is used. Note that this is a thin suit that provides reasonable protection from heat and cold conditions, but not enough to do an extensive spacewalk -- nor does it offer much in the way of radiation resistance. They are generally used in case of sudden vac conditions aboard a ship or space station. Any damage can cause a ***Simple Vacsuit*** to malfunction. The GM will determine if the character is having issues with it. This item does not require the ***Exosuit*** Skill in order to be used, but characters that *do* have it may be able to apply that Skill to problems which might arise while wearing one (*"Oops! Looks like I've sprung a leak. I wonder what I should do?"*)

<br />

#### Tool Kit
Cost: (Tool Kit) 500***Q***; (Replacement Tool Kit Supplies) 50***Q***

A must-have item for anyone with the ***Engineering*** Skill. Characters attempting to use ***Engineering*** to build, repair, or test something (and sometimes, just to figure out what's wrong), do so at a penalty if they do not have a ***Tool Kit***. In addition to actual tools, these kits have molecular cements (glue), patch tape, conductive creams, and other things, collectively known as Tool Kit Supplies. The ***Tool Kit*** can only be used up 20 times (successfully or not) before running out of Tool Kit Supplies, at which time the user works at a penalty when using it. (See ***Engineering***, in the chapter on Skills).
