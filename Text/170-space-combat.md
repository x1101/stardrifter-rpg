# Space Combat 

Space Combat is handled much the same way as character Combat, including
the use of Initiative (either Basic or Advanced), Skill rolls to hit,
and Armor considerations. Some elements, such as the Rating of a vessel,
bring in their own modifiers, on top of any Skill considerations or
situational modifiers. If a ship is provided with Point Defense (PD),
characters may have an opportunity to avoid certain types of attacks
entirely.

<br />

<div align="center">

**"Civilian class vessels are allowed to use deadly force if given no other choice, but must attempt to escape dangerous situations, rather than prolong them."**

**-Regulation 2864/B7 of the RMA Guidelines**

</div>

<br />

<div align="center">

![sun-planet-500x173](../Images/sun-planet-500x173.gif "An unexplored star system!")\

</div>

## Gunner 
Each combatant vessel must have a **gunner**. If a vessel has no
weapons, or active defensive systems such as PD, it is not considered to
be a combatant vessel (though it still may be the *victim* of one). A
gunner might be a trained or studied specialist, a dedicated Artificial
Intelligence, the pilot of the vessel, or, if worse comes to worst,
anyone willing to give it a try.

All attacks are based upon the Skill or Attribute score of the gunner,
as follows:

-   A human gunner's ***Gunnery*** Skill score (or substitute Attribute
    if they do not have this Skill). Many, though certainly not all, NPC
    gunners will have *Gunnery 1*, for a total score of 11.
-   A dedicated AI's ***Gunnery*** score. This can vary from AI to AI.
-   The human pilot's ***Piloting: Space*** Skill at **-4**. (If the
    pilot also has ***Gunnery***, then they are only at **-2** in
    Combat, if piloting the vessel and fighting at the same time.)

<br />

## Preparation 
Anticipating when a fight might break out between your vessel and
another is the hallmark of a smart gunner. Being prepared is often the
difference between life and death. Even before you know whether an enemy
is present for sure, preparation matters.

It is important to understand that certain aspects of preparation may be
considered hostile acts, in and of themselves. Regular monitoring of the
situational status (or ***S2***) from a gunnery
station, and the running of various combat simulations are just fine.
Attempting to acquire a target lock on a real vessel, painting one with
aim-assistant energy beams, or any similar acts of overt aggression or
intimidation are *illegal* in most parts of space, if performed without
good reason. Anything like this may be questioned later by **Route
Management Authority** or military investigators, and a failure to
satisfy them could have serious consequences for the gunner, the
commanding officers, and even the vessel's owners, up to and including
criminal charges.

By international law, anyone sitting in the gunner's seat of a civilian
class vessel is considered to be *as* responsible for the combat-related actions of that vessel as is the command staff. It is a gunner's right and responsibility, therefore, to
refuse an order to fire if they deem the act to be in violation of the
law.

<br />

## Detection and Targeting 
The first consideration of any potential space combat situation is
detection of the enemy. For Space Combat, this is dependent upon a
vessel's Sensors, which allow for detection at an absolute maximum range
of **Rating x 50,000 kilometers** in open space. This detection range
drops to **Rating x 10,000 kilometers** in space with large amounts of
solar interference, dust, or asteroids. Combinations of factors can drop
sensor range down further, even all the way to nothing, if it's bad
enough.

In atmosphere, a vessel's maximum sensor range also drops considerably,
down to **Rating x 100 kilometers** in Terran-normal air pressures. This
range drops even further under greater air pressure; if the weather is
bad; if large amounts of solar radiation are present; or if there is
particulate matter in the air (smoke, ash, heavy pollutants, etc.). As
with bad sensor conditions in space, combinations of these factors can
drop sensor ranges all the way down to zero.

Targeting requires that the gunner (human or AI) power up the vessel's
ship defense equipment, and acquire the enemy vessel in their sites.
Civilian class vessels are rather limited in the efficacy of their
targeting equipment, as the sole function of weaponry on commercial
vessels is self-defense, not the waging of war. Beefing up weapon
targetting range is a common (though highly illegal) after-market
modification.

Nominal targeting range represents only **25%** of the vessel's detection range. Enemy
vessels that are targeted within the first quarter of detection range do
not have any distance-related modifiers applied. Those that are outside
of this nominal targeting range can be fired upon, but with increasingly
negative modifiers.

<div align="center">

| **Detection Range** | **Targeting Range**                        |
| :---:               | :---                                       |
| 0-25%               | **Nominal**: No modifiers.                 |
| 26-50%              | **Difficult**: -3 to hit with all attacks. |
| 51-75%              | **Far**: -6 to hit with all attacks.       |
| 76-100%             | **Extreme**: -9 to hit with all attacks.   |

</div>

Enemy vessels beyond Extreme targeting range cannot be attacked using
standard methods, since they cannot be detected.

<br />

## Initiative 
Upon closing enough distance for both vessels to detect each other,
Initiative (either [Basic]{style="font-weight: bold;"} or
[Advanced]{style="font-weight: bold;"}) must be determined. Again, if
one vessel can detect the other, but not vice-versa for whatever reason,
the one that detects has Initiative. This continues each Round until the
other vessel is able to detect its enemy on sensors.

Space Combat is conducted much like normal character combat, as follows:

**Basic Initiative**: Each round, someone on the PCs' side rolls
**1d20**, and the GM rolls **1d20** for the NPCs. Low roll wins; re-roll
any ties. All characters on the winning side attack, followed then by
the the other side. Despite this ordering of the combatants, *all
attacks in a single round are considered to be happening more-or-less
simultaneously*. The dice are rolled simply to aid in playing through
and understanding the action.

The Basic Initiative system isn't complicated or precise, nor is it
especially realistic, but it's *more* realistic than breaking every
gunner's actions down into distinct sequential order. Put simply, it
somewhat simulates the frenetic madness of real space combat. It also
has the advantage of speeding up the fight.

**Advanced Initiative**: If a more abstract approach is desired, either
as a matter of general preference, or because a particular situation
requires granularity, the following method may be used. The Advanced
Initiative system has the advantage of precision, but it lacks realism,
and can slow combat down.

All gunners roll for Initiative, as defined in the section on *Advance
Initiative* in the chapter on **Combat**. Lowest number goes first, then the
next up the line, and so on. Ties are re-rolled. For purposes of
Initiative, an AI's Skill score is equal to the vessel's **Rating** (the
AI's ability to fight is only as good as the sensors and weapons it has
available, no matter how smart it might be).

<br />

<div align="center">

![gunner-400x280](../Images/gunner-400x280.gif "They're clearing the horizon...get ready!")\

</div>


## Attack
After Initiative has been determined, gunners may attack upon their
turn. To attack, a gunner performs a Skill Check by rolling
**1d20** versus ***Gunnery*** (or whichever
substitute Attribute is being used) for each attack. All systems may be
fired in the same round, if desired, but each system requires its own
roll to hit. Situational modifiers may well apply.

Those vessels which have some sort of **Point Defense** (PD) weapon system installed apply
their ***Rating*** score as a negative modifier to any missile attack rolls. PD does not defend
against DEW attacks, so the modifier is not applied against those
attacks. (See **Point Defense**, below.)

<br />

#### DEWs
Directed Energy Weapons of all sorts exist in this future, but for
simplicity's sake in the Base Rules, we will consider them all as
being more-or-less the same thing: a powerful beam of charged particles
or waves. DEW systems get one attack each per round.

DEWs do ***3d100 x the attacking ship's Rating*** in
damage to any vessel it hits. That is, **three** rolls of a
**1d100**, added together, then this amount is
**multiplied** by the attacker's **Rating**.

<br />

#### Missiles
Civilian class missiles are installed in packs or cylinders, and when
fired, the entire pack is launched at once. For game purposes, this is
considered a single attack From that point on, they can only be
controlled as a single unit.

Missiles carry enough onboard fuel to run at full burn out to the
furthest end of a vessel's Detection Range. They can be throttled down
by the attacking gunner, in order to save on fuel; they can be shut off
entirely, allowing them to coast along ballistically; they can even be
parked somewhere, and then reactivated at a later time. However they are
used, a failed roll to hit means the missiles have gone off course, or
have otherwise missed the target vessel, and have self-terminated safely
somewhere out of range.

Each successful missile attack does ***1d1000 x the attacking vessel's
Rating score***, in HP damage to the target.

Missile Packs are preloaded by a manufacturer or third-party service
provider, and cannot, by law, be *re*loaded by the end user. Whenever a
missile attack is made, whether it is successful or not, the pack is
emptied. It is thereafter useless until reloaded by a licensed and
authorized weapons dealer at a station or spaceport somewhere. Missile
Packs cost the location's **Cost of Living** x 1000***Q*** to replace. (See the chapter on **Money**, under *Docking Fees*.)

<br />

#### Point Defense
Point Defense (PD) refers to systems designed to intercept and
destroy/disable incoming missiles. It is completely ineffective against
DEWs and mass-effect attacks.

PD can take one of three forms:

-   ***Rapid-Fire Cannon*** (RFC), which is a 20mm rotary kinetic slug
    autogun linked directly to the vessel's sensors and Gunnery systems
    for targeting.
-   ***Single-Fire Cannon*** (SFC), which ejects a 150mm ball that
    rapidly expands into a cloud of polynium pellets, acting as flak.
-   ***Neutral Particle Cannon*** (Neupac), which is a flavor of DEW specifically
    designed for close-order attacks, utilizing a low mass/high velocity
    stream of particles, hitting with great collective force; it is
    linked to the defending vessel's sensors and Gunnery suite. (Can be used for close-range offensive attacks against smaller vessels.)

Regardless of the exact form, all PD systems induce a **-4** penalty upon the
*attacking* gunner's ability to hit with their missile attack.

<div align="center">

![dead-spaceman-300x213](../Images/dead-spaceman-300x213.gif "The aftermath of war!")\

</div>
