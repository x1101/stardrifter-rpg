# Attribute Checks, Skill Checks, and Save Checks

Most actions are simple, never requiring any sort of Check to see if
they succeed. When a character attempts to do something difficult,
however, especially when under stress (being chased, being shot at,
defusing a bomb, etc.), an Attribute, Skill, or Save Check is required.
The GM determines when situations call for a Check, and which one is
required.

All Checks in this game are performed with **1d20**. If the result of
the roll is equal to or less than whichever score is being compared, the
Check is successful. The GM may, and often will, determine that
situational modifiers are called for, and if so, they are applied to the
character's score, be it an **Attribute**, **Skill**, or **Save**.

<div align="center">

![running-spaceman-250x215](../Images/running-spaceman-250x215.gif "That's the emergency klaxon!")\

</div>

***

## Attribute Checks

These are called for when a character attempts to do something requiring
a specific Skill that they don't possess. The character must roll **1d20 against that Skill's associated Attribute, often with penalties. If the number on the die is equal to or
less than their Attribute Score, the attempt is successful. The GM may
apply modifiers to the score being rolled against, and might also have a
very different definition of *success* than do the players.

> **Example**: *Jerron is a freelance computer spesh working to uncover
> some data from a secure network owned by a shady corporation. He can
> only gain access to the system from an adjunct office, tucked away in
> the spoke of a large space station. It's unguarded but does have an
> alarm. Jerron is able to get past this alarm by shutting off power to
> the small alcove in which the office is located. The door, itself, is
> another problem. He can't break it down: it's too strong, and no one's
> supposed to even know he was there. He has to pick the lock, but does
> not have any mechanical Skills (in this case, **Engineering**). The GM
> determines that DEX is the appropriate Attribute to roll against. In
> this case, Jerron's DEX is a 12. Because he doesn't have any proper
> training, however, the GM imposes a -1 penalty on his Attribute score,
> making it an 11 for this purpose. The player rolls **1d20**, and gets
> an 11, exactly. That's nearly a failure, but the GM determines that
> the lock has been picked.*

> **Example**: *After the digital attack has been implemented, Jerron
> must now leave the way he came, relocking the door so that his
> presence here remains undetected later. Normally, this would just
> require the same modified DEX roll as before, but a few hours have
> passed, wherein the heaters in the deck and walls of the alcove have
> been off (remember, the power is out). It's actually cold out there
> now; condensation has formed, and water has gotten inside the lock.
> The GM determines that the water has made the locking mechanism more
> difficult to mess with, and imposes an additional -2 penalty on the
> attempt, on top of the -1 for Jerron not knowing how to properly pick
> a lock to begin with, bringing the total penalty now to -3. This time,
> Jerron's player rolls a 12, which is equal to his DEX. Under other
> circumstances, this would have been a success; because of the
> modifiers, however, his effective DEX for this Check is actually 9. A
> 12 rolled on **1d20** is higher than 9, so Jerron fails, and cannot
> relock the door. He's able to turn the power back on in the alcove,
> which re-arms the security system, but the door itself is not locked,
> very possibly tipping off the company.*

***

## Skill Checks

When Skill Check is called for by the GM, a character rolls **1d20**
against their appropriate Skill. Modifiers may apply. If a number equal
to or less than their Skill score is rolled on the die, the attempt is
successful. If a character does not possess the correct Skill, they must
roll versus its associated Attribute, usually with penalties.

> **Example**: *Jerron decides that his lack of knowledge about locks
> and mechanical systems is a problem. After adventuring for a bit, he
> gains some CPs and decides to learn a new Skill. He ends up with
> **Engineering** 1, using INT as the associated Attribute, which gives
> him a score of 15 in this Skill. Life has a way of being circular,
> sometimes, and Jerron gets re-hired to perform the exact same
> operation as before, using the exact same remote office. Apparently,
> failing to relock the door last time was taken to be a mistake on the
> part of an employee, since nothing else seems to have been disturbed.
> Nonetheless, the incident apparently made it clear that improvements
> were required, so the company went ahead and replaced the entire lock
> mechanism of the door. It will now not even* shut *unless it's locked;
> the door is on a spring, and it'll pop right open unless it's closed
> firmly and specifically locked. The power and alarm systems are just
> as easily defeated as before, since there has been no upgrade to them.
> If the door still had same mechanism, the GM might have decided
> picking it would just be a straight-up Skill Check, with no penalties
> involved, since Jerron has seen this lock before, and now knows the
> basics of such simple mechanisms. This new lock is unexpected snag,
> though, so there is once again a -1 penalty to the job, bringing
> Jerron's **Engineering** 1 Skill down to 14. That's still better than
> the modified DEX roll of last time, so Jerron's player rolls **1d20**,
> and gets a 13. This would have been a failure last time. This time,
> it's a success. Let's hope he rolls just as well when it's time to
> leave!*

<br />

<div align="center">

![rocket-battle-300x218](../Images/rocket-battle-300x218.gif "Fire all atom ray cannons!")\

</div>

***

## Save Checks

Saves are last ditch statistics to roll against in order to reduce or
avoid the effects of otherwise unavoidable situations. Such situations
can take the form of almost anything, ranging from lethal explosions, to
surviving a spell of vacuum exposure without a suit; from being conned
by a grifter, to resisting the effects of brainwashing.

Like other types of Checks, Saves are rolled with **1d20**. Equal to or
less than the Save number means success; greater than means failure; and
modifiers may well apply.

Save Checks come in two varieties, **Mental** and **Physical**, and
their scores are obtained as follows:

-   **SAVE: Mental**: Add up your character's INT, WIS, CHA, and divide
    by 3 (rounding up). Put this number under **SAVE: Mental** on your
    Character Sheet.
-   **SAVE: Physical**: Add up your character's STR, CON, DEX, and
    divide by 3 (rounding up). Put this number under **SAVE: Physical**
    on your Character Sheet.

Save Check scores can be improved over time by increasing a character's
Attributes via CPs, which are obtained through adventuring. When an
Attribute is permanently increased (or *decreased*, it's a dangerous
galaxy), Save Check scores must be re-calculated using the above
methods.

> **Example**: *Feeling oddly immune to failure due to his new Skill,
> Jerron takes the same job once again, figuring it's easy money. Little
> does he know that the occasional power failure in this alcove has been
> noticed by station maintenance, who had to file a report with security
> since an alarm system is hooked up there. Security is now alerted
> immediately if there's a power loss. He doesn't get too far this time
> before two guards show up. They call for the elevators to be locked
> off in order to hamper his escape efforts, so instead, Jerron tries
> using the emergency ladder to go down to the previous level (there are
> no stairs in the struts of this station). Determining this to be a
> high-pressure situation, the GM has Jerron's player roll versus his
> DEX score with a penalty of -2, otherwise, he'll slip as he scrambles
> down. Again, Jerron's DEX is 12; with the imposed -2, that's a 10,
> which brings him down to an even chance of success on **1d20**.
> Jerron's player rolls a 16. Fail! It's a loooong way down, which makes
> this an otherwise unavoidable doom, so the GM states that a **SAVE:
> Physical** Check is required. Jerron's score in this is 13. The GM
> decides to be more lenient than I would probably be under the
> circumstances, and does not impose any negative modifiers. The player
> rolls an 8 on **1d20**, which is a success. The GM determines that
> Jerron tries to climb down, but slips and falls for a few meters,
> taking **1d6+2** points of damage as he bounces down the metal
> rungs, before catching one and stopping his fall. Yes, he was hurt
> from the partial fall, but at least he didn't drop to his death.
> Jerron can now climb down carefully to the next level, or climb back
> up into the waiting arms of the guards.*

> **Example**: *It turns out the GM is not nicer than I am, after all,
> because Jerron's brush with death has left some trauma. He begins
> having nightmares about falling, and grows increasingly acrophobic.
> Time goes by, and dire circumstances within the game require Jerron to
> walk along a very high catwalk inside a steaming, fume-filled factory.
> Jerron's new-found fear of heights, coupled with the fumes and smoke
> in the place, are major issues; the GM determines that Jerron must
> first make a **SAVE: Mental** Check, to push past this fear. The player rolls a 14. This is more than the
> **SAVE: Mental** score on Jerron's Character Sheet, which is a 13.
> That's a fail. Jerron is too overcome with anxiety to get very far,
> and is forced to turn back.*

> **Example**: *Later on, the factory closes for the day, and the fumes
> and smoke abate after a few hours. Circumstances with the catwalk have
> changed, in other words, so the GM rules that Jerron may make another
> attempt to cross it. His fear of heights is still very much present,
> so another **SAVE: Mental** Check is required. This time, Jerron's
> player gets a 7 on **1d20**. That's under his **SAVE: Mental**
> score, making this a success. Jerron overcomes his fear, and makes it
> across safely.*
