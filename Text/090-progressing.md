# Progressing In The Game

**Character Points**, or **CP**s, are used with one of the
previously-mentioned methods for character creation, but they are also
used throughout the rest of the game, for gradual improvement.
Regardless of the exact method of generating Attributes, characters
progress in Stardrifter by adventuring and earning CPs, which are then
expended upon the improvement Attributes or Skills.

In Stardrifter, at the end of each adventure (as opposed to the end of
each game session), characters receive a small number of CPs. These are
awarded by the Game Master for facing enemies, attempting tasks,
pursuing large-scale goals, creative problem solving, and excellent
role-playing.

*Progression* is divorced from *achievement in this game. We learn as
much, or more, from our failures in life, as we do from our successes.
Characters who make honest attempts at the challenges they face, whether
successful or not, have invariably learned from the experience. Players
should set their own goals for their characters, rather than having the
rules or the GM set them.

At any rate, for the sake of smooth gameplay, ***no more than 3 CPs*** may
be awarded to any character, at any one time.

#### Possible Character Point awards per adventure:

1.  **1 CP**: Surviving an adventure, however it turns out.
2.  **1 CP**: Actively pursuing main goals.
3.  **1 CP**: Creative problem solving.
4.  **1 CP**: Good role-playing.
5.  **1 CP**: Miscellaneous reasons (GM's choice).

CPs can be expended, one-for-one, upon any of the following:\

-   Increasing one **Attribute** by **1 point** per adventure. If that
    **Attribute** is CON, this increase may also adjust their Stamina or HP.
-   Learning up to three new **Skills**, each of which must start at **Level 1** (no one
    begins as an expert).
-   Increasing one or more previously known **Skills** by up to **3 points** per
    adventure.

CPs cannot be saved, but must be used up before the character is played
again.

Expending CPs to improve Attributes does *not* affect any Skills already
known. To increase known Skills, a player uses the character's CPs on
them directly. New Skills, however, are learned using whatever the
current associated Attribute score is as its base starting number. If
spreading CPs between Attributes and Skills, therefore, it might be a
good idea to increase the Attribute first, so that the new Skill can
take advantage of it as a starting **Skill score**.

<div align="center">

![rocket-ready-for-launch-68x132](../Images/rocket-ready-for-launch-68x132.gif "We are go for launch!")\

</div>

Any additions made to Attributes might reflect memory exercises (INT);
practicing meditation (WIS); taking classes in public speaking (CHA);
lifting weights (STR); jogging (CON); or practicing juggling (DEX). In
this future, a character could also get genetic or cybernetic work done
that could account for some or all of these changes. A player may
describe the exact process as they wish, reflecting the addition of a CP
to one of their character's Attributes.
