# Damage and Healing

An unfortunate side-effect of adventuring across the stars is that a
character will, occasionally, find themselves a little worse for wear.
Damage can come from almost anything, ranging from accidents, to combat;
from toxins, to radiation exposure. This section deals with the way
damage is recorded, and how it is applied in the game.

Player characters (PCs) are different from non-player characters (NPCs),
in that PCs have a Stamina (STAM) score, while most NPC's do not.
Stamina represents the heroic ability to shrug off a certain amount of
injury and proceed on. It's something that sets player characters apart
from the rest of the universe.

Any damage to a PC comes off their STAM first. When that reaches zero,
any further damage is subtracted from their Hit Points (HP).

<div align="center">

![space-doctor-250x209](../Images/space-doctor-250x209.gif "You're healthy as a space horse!")\

</div>

Another element that's only for player characters is that, for any
attack that reduces their HP (*not* their STAM), they are allowed a
**SAVE: Physical** to reduce that damage by **1/2** (**1/4** for Xmil characters), rounded up, with
**1** HP being the minimum. The majority of NPCs do not get this
capability, though some *might*, at the GM's discretion (especially
tough henchman, infamous mercenaries, pirate captains, etc.)

<br />

## Stamina
All damage comes off a character's STAM score first. These represent the
character's ability to take a few hits without slowing down, and to
shrug off a certain amount of damage like a true adventurer should. STAM
points return at a rate of **1** point for every five minutes of rest.
No STAM points can be regained until *all* HP have been regained.

<br />

## HP
Once a character's STAM points are exhausted, all damage comes off a
character's HP. Should a character be reduced to **zero** (0) HP, that
character is unconscious (extra damage from the hit that brought them to
zero is ignored). They will continue to lose **1** HP per round, down to
a negative number equal to their CON, at which point they are dead.
Characters at **0** HP or below, whether
stabilized or not, are automatically killed if they take any additional
damage; this is in effect until they reach at least **1** HP. If they are tended to by someone with the
***Medico*** Skill (or by someone without it who rolls very well!), they will be stabilized, but
still unconscious. Further medical attention, or natural healing, will
be required to bring them to 1 HP or better, at which time they will
regain consciousness, but be Stunned for **1d4** additional hours. Under normal
circumstances, HP returns at a rate of **1** point per **24** hours of rest. Certain drugs
and medical procedures can mitigate the process of healing. (See
***Medico*** in the chapter on **Skills**.)

> **Example**: *A couple of people -- one a player character, and the
> other an NPC, by the names of **Belmont** and **Nana** -- begin fighting
> each other. Nana is pretty tough, having learned martial arts at a
> young age. She uses her **Combat: Hand-to-Hand** 3 for a total score of
> **17** against Belmont, who has always relied upon his STR of **16**, along with
> a bad attitude, to intimidate and brutalize people. Nana isn't
> having any of it, and attacks him with a roundhouse kick. Her player
> rolls **1d20** against her **Combat: Hand-to-Hand**, getting a
> **4** on the die. Easy hit! Nana rolls **1d4+2** to determine damage, getting a
> **3**, plus that **2**, for a total of **5**. Unfortunately for Nana, **she's** the
> NPC, and this damage comes off Belmont's STAM, revealing this
> suddenly to be a tougher fight than she was expecting! Belmont is
> nothing more than a bruiser. His player rolls an Attribute Check on
> **1d20** versus his STR, with a **-2** 
> penalty because he is untrained. That makes His STR a modified total
> of **14** for the purpose of fighting. Not
> as good as Nana's trained Skill, perhaps, but still better than
> average. The player rolls a **9** on the
> die, which is a hit, and Nana takes **1d4+2**, for a total of
> **4**. She only has
> **10** HP, so this is a serious hit!
> Initiative is rolled again, and Belmont wins. This time, he rolls a
> **13**. Even with his -2 penalty, that's a
> hit, and he inflicts **5** more HP in
> damage to Nana. This is looking grim. Nana swings an uppercut, and
> the GM rolls **1d20**, getting an **11**.
> That's a hit, for **1d4+2** damage. The roll is a natural
> **4**, plus that **2**, making a total of
> **6** HP of damage, all of which is still
> coming off Belmont's STAM (he's a big guy). But wait! a natural
> **4** with the **Combat: Hand-to-Hand**
> Skill means Belmont must roll a **SAVE: Physical Check**, or be
> Stunned for **1d4** rounds. His Save score is
> **15**, which is really good. His player
> rolls **1d20**, and gets a **16**,
> however, which is not so good. Belmont is Stunned, reducing his
> Movement speed, and all **Attribute**,
> **Save**, and **Skill Checks**, by
> **1/2** until the effect wears off. He
> cannot win Initiative while Stunned, so it's Nana's decision what to
> do here. With only **1** HP left, she
> expresses the better part of valor, having smacked Belmont so hard
> he won't be able to follow for a while. When the Stun wears off,
> though, and after about an hour's time to shake off the rest of the
> damage (that is, to recover all his STAM), he'll be angry, and just
> as tough as ever. Nana, on the other hand, will need a
> week-and-a-half to recover her HP naturally! Medico attention could
> greatly increase the healing rate for both combatants, but it's
> clear that the life of an NPC can be tough.*

<br />

#### Healing Stun Effects
Stun may be removed from a character through a successful ***Medico***
Skill check, either performed by the victim, themself (who is rolling at
**1/2** normal, because of the Stun), or by
someone else. Success indicates that the proper combination of
stimulants, analgesics, and anti-inflammatory drugs have brought this
character back around. Additionally, and healing that goes to HP or STAM
automatically counteracts Stun. Otherwise, the Stun continues until it
wears off naturally (usually a matter of rounds). No STAM points will
return until Stun has worn off, or until Medico attention has been
obtained. (See ***Stun***, in the chapter on **Combat**.)

<br />

#### Impact Damage
This is something of a special case, as it refers to a character
impacting another surface at high speed, or that surface impacting the
character. This may be from a variety of sources, including falling,
bumping into a wall, or being hit by something of substantial size.

Extremely deadly impacts, such as being hit by a bus, or falling off a
cliff, may be considered automatically lethal, at the GM's discretion,
but if so, the character is still allowed to roll a **SAVE: Physical** Check, in order to somehow avoid
an instant death.

Impacts come in three speeds: **Low**, **Moderate**, and **High**. Surfaces come in three hardnesses:
**Soft**, **Medium**, and **Hard**.


> **Example**: ***Soft** surfaces might include padded walls, thick carpets, or trampolines. **Moderate** surfaces might include other people, a fruit stand full of apples, or a pool of water from a few meters up. **Hard** surfaces might include metal bulkheads, a car, or the ground.*

> **Example**: ***Low** speed might be anywhere from .5 to 8 kph. **Medium** speed might be from 9 to
> 20 kph. **High** speed might be from 21 to 40 kph or up.*

What exactly constitutes *any* of these factors is up to the GM, and may be determined on a case-by-case basis.

The damage rolled for a character on impact depends upon the interplay
between these factors, as listed in the box below:

<div align="center">

|                 | **Surface: Soft** | **Surface: Med.** | **Surface: Hard** |
| :---:           | :---:             | :---:             | :---:             |
| **Speed: Low**  | -                 | 1d4+2             | 1d4+3             |
| **Speed: Mod.** | 1d4+1             | 2d6+2 (\*-2)      | 3d6+3 (\*-3)      |
| **Speed: High** | 1d6+1 (\*-3)      | 2d8+2 (\*-4)      | 3d8+3 (\*-5)      |

</div>

<br />

An **asterisk** means the character must also make a **SAVE: Physical**, or be ***Stunned*** for **1d4** rounds. The minus numbers represent negative modifiers to the character's **SAVE: Physical** score when they roll.

> **Example**: ***Leon** falls out a window and lands on **Anita**, who was just walking by, minding her business. Anita is a person, so she is considered a **Medium** surface, The window is 5 meters up, but Anita is standing upright, so Leon will come in contact with her shoulders after a fall of about 3.5 meters. We'll calculate from there. At the end of **3.5** meters, Leon is moving roughly **30 kph**, which is **High** speed. The GM rolls **2d8+2** for Leon. It comes out to a total of **9 HP** of damage for him, and he also rolls a **SAVE: Physical** at -4. His **SAVE: Physical** score is 14. -4 makes that a **10**. Leon's player rolls **1d20**, and gets an **11**. Normally, that would save, but the
> modifier means it doesn't this time. He is fully **Stunned**. The GM rolls a **1d4** for that, and determines he is out of it for **3** rounds. Anita gets her own rolls for damage and Stun. However it turns out for her, Leon is just laying on the sidewalk, battered and moaning.*

The above is an example of moderately detailed gameplay with a little
bit of Terran-normal falling speed calculation involved. In practice, a
GM might simply make this call entirely off-the-cuff, and probably
should. Never let the rules get in the way of good fun.

<br />

#### Vacuum Exposure
Exposure refers to direct contact with a vacuum or near-vacuum
environment. It's not the cold, the heat, or the radiation that gets
you: if you don't have a pressurized bulkhead or working spacesuit
between you and the vacuum of space, you will suffocate very, very
quickly. You cannot hold your breath in space; the pressure differential
between your lungs and the nothingness outside makes that flat-out
impossible. You simply start to die.

<br />

<div align="center">

## Space is murderous. It's very touch kills.

</div>

<br />

Any character exposed to vacuum experiences the following effects ***each
round*** the exposure continues:

-   Roll a **SAVE: Physical** with a **-5** modifier, or be
    **Stunned** (Spacers roll at **-2**).
-   Take **1d12+2** points of damage.

Remember that being Stunned a second time, while you are already Stunned, means you are automatically unconscous. Also remember that any single damage infliction which brings you to **zero** (0) HP means you are automatically losing **1** HP per round thereafter; if you take any further damage whatsoever before you are brought back up to at least **1** HP, you immediately die. (See the chapter on **Combat**).

<br />

#### Drowning
Somewhat similar to Vacuum Exposure, drowning kills by suffocation. Any
character who has taken a deep breath before entering the water
(assuming it's water; it could be a lot of things) may remain under for
a number of **minutes** equal to **1/4** of their
**CON** Attribute score, rounded up. After
this, they experience the following effects each round that they are
under water (*not* each minute; remember,
rounds are subjective, so this part might only last a couple of
seconds):

-   Roll a **SAVE: Physical** with a **-3** modifier, or be ***Stunned***.
-   Take **1d12+2** points of damage.

Those with the **Athleticism**] Skill have
several advantages here. First off, they know how to swim. If they are
not under any servere stress (being shot at, being attacked by a shark,
whatever), they don't need to make any kind of roll to swim
successfully; they know how to do it, so they just do it. Secondly, if
drowning should become a factor, the rules above still apply, except
that these characters can make an Athleticism Skill check to avoid the
effects for an additional minute beyond **1/4** their
**CON**. After this, they can make an
additional Skill check at **-2**, if this is
successful, they avoid the drowning effects for
*another* minute. After this, they may get
another minute with a successful roll at
**-4**, then **-6**, then **-8**, etc. If they fail their Skill
check at any point, they begin to drown in the same fashion as above.
However, even if ***Stunned***, they may
still try to roll versus their Athleticism (now at
***1/2*** their normal Skill
level, of course, because of the Stunning effect), in order to regain
control and survive another minute, with the negative modifiers on their
Skill continuing on from wherever they left off.
