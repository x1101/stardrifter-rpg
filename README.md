<div align="center">

# Stardrifter
*a space opera role-playing game*

by

**David Collins-Rivera**

[Latest Rules - EPUB](https://gitlab.com/x1101/stardrifter-rpg/-/jobs/artifacts/live/raw/stardrifter.epub?job=build-job)

[Latest Rules - PDF](https://gitlab.com/x1101/stardrifter-rpg/-/jobs/artifacts/live/raw/stardrifter.pdf?job=build-job)

![Cover Image](./Images/01-front-cover-final-2550x3000.jpg)

</div>
